Read and Write EDID
===================

Read and write EDID data over an I2C bus.

Usage
-----

Load the Linux kernel module:

    modprobe i2c-dev


Adapt the I2C bus device `/dev/i2c-4` in the examples below
to whatever matches your hardware setup.

**Note:** Be very careful to write to the correct bus!
Failing to do so may seriously damage your hardware!


To read an EDID, run (as root):

    ./edidread -d /dev/i2c-4 > edid.bin

To write an EDID, run (as root):

    ./edidwrite -d /dev/i2c-4 < edid.bin


**Note:** The `edidread` tool writes binary data to stdout which
is potentially dangerous. Redirect the output into a file or pipe.

For further usage information, run the comands with `--help`.

Notes
-----

Validate the EDID binary blob with the `edid-decode` tool:

    edid-decode -c < edid.bin

Links
-----

* https://git.linuxtv.org/edid-decode.git/about/

use clap::Parser;

fn hex_address(a: &str) -> Result<u16, std::num::ParseIntError> {
    u16::from_str_radix(a.trim_start_matches("0x"), 16)
}

/// Command line argument definitions.
#[derive(Parser, Debug)]
#[command(version, about, long_about = None, next_line_help = true, arg_required_else_help(true))]
pub struct Args {
    /// The linux i2c device file, something like /dev/i2c-*.
    #[arg(short, long)]
    pub device: String,

    /// The chip slave address, usually 0x50.
    #[arg(short, long, default_value = "0x50", value_parser = hex_address)]
    pub address: u16,

    /// Number of 128-byte blocks to read, defaults to autodetect from data.
    #[arg(short, long, default_value = None)]
    pub blocks: Option<usize>,
}

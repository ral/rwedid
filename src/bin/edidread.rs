use clap::Parser;
use rwedid::args::Args;
use rwedid::i2c::read_from_bus;
use rwedid::io::block_by_block_read;
use rwedid::std::write_to_stdout;

fn main() -> Result<(), std::io::Error> {
    // Parse command line arguments
    let args = Args::parse();

    // Read the data from the chip
    let bytes = block_by_block_read(args.blocks, |o, l| {
        read_from_bus(&args.device, args.address, o, l)
    })?;

    // Write binary blob to stdout
    write_to_stdout(&bytes)
}

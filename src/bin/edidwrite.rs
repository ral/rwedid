use clap::Parser;
use rwedid::args::Args;
use rwedid::i2c::write_to_bus;
use rwedid::io::block_by_block_read;
use rwedid::std::read_from_stdin;

fn main() -> Result<(), std::io::Error> {
    // Parse command line arguments
    let args = Args::parse();

    // Read binary blob from stdin
    let data = block_by_block_read(args.blocks, |_, l| read_from_stdin(l))?;

    // Write the data to the chip
    write_to_bus(&args.device, args.address, 0x00, data.as_slice())
}

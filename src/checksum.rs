/// Compute the checksum over the bytes of a block.
#[allow(clippy::cast_possible_truncation)]
pub fn checksum(bytes: &[u8]) -> u8 {
    bytes.iter().fold(0u32, |x, y| x + u32::from(*y)) as u8
}

/// Check if the checksum is valid.
pub fn valid_checksum(bytes: &[u8]) -> bool {
    checksum(bytes) == 0x00
}

/// Print the checksum to stderr.
pub fn print_checksum(bytes: &[u8]) {
    let cs = checksum(bytes);
    eprintln!("  Checksum is: {cs}");
}

//! Read and write EDID data over an I2C bus.

/// Command line argument definitions.
pub mod args;
/// Checksum functions for EDID.
pub mod checksum;
/// Interact with an i2c device.
pub mod i2c;
/// Abstract io for EDID blocks.
pub mod io;
/// Read from stdin and write to stdout.
pub mod std;

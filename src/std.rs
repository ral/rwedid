use std::io::{self, Read, Write};

/// Read a fixed-size block of binary data from stdin.
pub fn read_from_stdin(data_length: usize) -> Result<Vec<u8>, std::io::Error> {
    // Buffer to read into
    let mut data = vec![0; data_length];
    // Read from stdin
    io::stdin().read_exact(data.as_mut_slice())?;
    Ok(data)
}

/// Write a fixed-size block of binary data to stdout.
pub fn write_to_stdout(data: &[u8]) -> Result<(), std::io::Error> {
    // Write data to stdout
    let mut stdout = io::stdout().lock();
    stdout.write_all(data)
}
